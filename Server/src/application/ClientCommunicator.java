package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;

import javax.swing.JTextArea;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class ClientCommunicator implements Runnable
{
	private Socket clientSocket;
	private JTextArea textArea;
	private PrintWriter writer;
	private BufferedReader reader;
	
	public ClientCommunicator( JTextArea textArea, Socket clientSocket )
	{
		this.textArea = textArea;
		this.clientSocket = clientSocket;
		
		textArea.append( "Client " + this.clientSocket.getInetAddress() + " joined\n");
		
		Thread thread = new Thread( this );
		thread.start();
	}
	
	@Override
	public void run()
	{
		try
		{
			this.connect();
			this.converse();
		}
		
		catch( IOException e ) 
		{ 
			e.printStackTrace();
		}

		this.close();
	}

	public void connect() throws IOException
	{
		this.writer = new PrintWriter(this.clientSocket.getOutputStream(), true);

		this.reader = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
	}
	
	private String processInput( String input )
	{
		String answer = "Could not parse sent json string";
		
		JSONObject jsonObject = (JSONObject) JSONValue.parse( input );
		
		if( jsonObject != null )
		{
			if( jsonObject.containsKey("location") )
			{	
				answer = getAnswer();
			}		
		}
		
		return answer;
	}
	
	public String getAnswer()
	{
		JSONArray array = new JSONArray();
		
		String ding = "LOCATION RECEIVED";
		JSONObject obj = new JSONObject();
		obj.put("location", ding);
		array.add(obj);
		return array.toJSONString();
	}

	private void converse() throws IOException
	{
		String line = null;
        String outputLine = null;
	    
        line = reader.readLine();
	    if (line != null) 
	    {
	        textArea.append( "Received: " + line + "\n" );
	        outputLine = processInput( line );
	        writer.println(outputLine);
	    }  
	    this.close();
	}
	
	private void close()
	{
		try
		{
			this.writer.close();
			this.reader.close();
			this.clientSocket.close();
		}
		
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
